package com.example.nguyenkhoalu.location_app.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.nguyenkhoalu.location_app.CustomerFeedback.FeedbackInfor;
import com.example.nguyenkhoalu.location_app.R;

import java.util.List;

public class FeedbackAdapter extends ArrayAdapter<FeedbackInfor>
{
    private Context context;
    private int resource;
    private List<FeedbackInfor> listFeedback;

    public FeedbackAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<FeedbackInfor> listFeedback) {
        super(context, resource, listFeedback);
        this.context = context;
        this.resource = resource;
        this.listFeedback = listFeedback;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        ViewHolder viewHolder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.feedback_information,parent,false);
            viewHolder = new ViewHolder();
            // viewHolder.tvID = (TextView)convertView.findViewById(R.id.tv_id);
            viewHolder.tvNameCustomer = (TextView)convertView.findViewById(R.id.tv_nameCustomer);
            viewHolder.tvEmail = (TextView)convertView.findViewById(R.id.tv_email);
            viewHolder.tvRating = (TextView)convertView.findViewById(R.id.tv_rating);
            viewHolder.tvContent = (TextView)convertView.findViewById(R.id.tv_content);
            //viewHolder.tvType = (TextView)convertView.findViewById(R.id.tv_type);

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        FeedbackInfor feedbackInfor = listFeedback.get(position);
        //viewHolder.tvID.setText(String.valueOf(feedbackInfor.getID()));
        viewHolder.tvNameCustomer.setText(String.valueOf(feedbackInfor.getNameCustomer()));
        viewHolder.tvEmail.setText(String.valueOf(feedbackInfor.getEmailCustomer()));
        viewHolder.tvRating.setText(String.valueOf(feedbackInfor.getRatingResult()));
        viewHolder.tvContent.setText(String.valueOf(feedbackInfor.getContentFeedback()));
        //viewHolder.tvType.setText(String.valueOf(feedbackInfor.getType()));

        //Log.d("Name: ", String.valueOf(feedbackInfor.getNameCustomer()));
        //Log.d("Email", String.valueOf(feedbackInfor.getEmailCustomer()));
        //Log.d("Rating", String.valueOf(feedbackInfor.getRatingResult()));
        // Log.d("Content", String.valueOf(feedbackInfor.getContentFeedback()));

        return convertView;
    }

    public class ViewHolder
    {
        private TextView tvID;
        private TextView tvNameCustomer;
        private TextView tvEmail;
        private TextView tvRating;
        private TextView tvContent;
        //private TextView tvType;
    }
}
