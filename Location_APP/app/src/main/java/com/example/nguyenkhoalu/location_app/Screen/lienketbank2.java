package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

import java.util.Random;

public class lienketbank2 extends Activity {


    EditText txtstk, txtst, txthtct, txtnhh, txtmxn;
    Button btnxacnhan;
    TextView txtcode;
    int ram;
    TAIKHOAN tkCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lienketbank2);
        txtstk = (EditText) findViewById(R.id.txtstk);
        txtst = (EditText) findViewById(R.id.txtst);
        txthtct = (EditText) findViewById(R.id.txthtct);
        txtnhh = (EditText) findViewById(R.id.txtnhh);
        txtmxn = (EditText) findViewById(R.id.txtmxn);
        txtcode = (TextView) findViewById(R.id.txtcode);
        btnxacnhan = (Button) findViewById(R.id.btnxacnhan);
        Bundle bR = this.getIntent().getExtras();
        tkCurrent = (TAIKHOAN) bR.getSerializable("TaiKhoang");
        Intent i = getIntent();
        String username = i.getStringExtra("username");
        String bank = i.getStringExtra("bank");
        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);

        sqLite.query("CREATE TABLE IF NOT EXISTS BANK(SOTAIKHOAN VARCHAR PRIMARY KEY, MASOTHE VARCHAR,HOTEN VARCHAR,NGAYHETHAN VARCHAR, TYPEBANK VARCHAR)");
        Random rd=new Random();
        ram =rd.nextInt((999999-99999) + 1 );
        String num = "" + ram;
        txtcode.setText(num);
        btnxacnhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stk, st, htct, nhh, mxn;
                stk = txtstk.getText().toString();
                st = txtst.getText().toString();
                htct = txthtct.getText().toString();
                nhh = txtnhh.getText().toString();
                mxn = txtmxn.getText().toString();
                String numoncl = txtcode.getText().toString();
                if(stk.equals("")||st.equals("")||htct.equals("")||nhh.equals("")||mxn.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Bạn chưa nhập đày đủ thông tin", Toast.LENGTH_LONG).show();
                    ram =rd.nextInt((999999-100000 + 1) + 100000);
                    String numon = "" + ram;
                    txtcode.setText(numon);
                }
                else {
                    String sqlite = String.format("SELECT * FROM BANK WHERE SOTAIKHOAN = '%s'", stk);
                    Cursor cursor = sqLite.getdata(sqlite);
                    String name = "";
                    while (cursor.moveToNext()) {
                        name = cursor.getString(2);
                    }
                    if (name.equals("")&& mxn.compareTo(numoncl) == 0) {

                        String sql = String.format("INSERT INTO BANK VALUES('%s','%s','%s','%s','%s')", stk, st, htct, nhh,bank);
                        String sqlup = String.format("UPDATE TAIKHOAN SET SOTK = '%s' WHERE USER = '%s'",stk,username);
                        sqLite.query(sql);
                        sqLite.query(sqlup);
                        Toast.makeText(getApplicationContext(), "Liên kết thành công", Toast.LENGTH_LONG).show();
                        txtstk.setText("");
                        txtst.setText("");
                        txthtct.setText("");
                        txtmxn.setText("");
                        txtnhh.setText("");
                        ram = rd.nextInt((999999 - 100000 + 1) + 100000);
                        String numon = "" + ram;
                        txtcode.setText(numon);
                        tkCurrent.setNumofAcount(stk);
                        Intent i = new Intent(getApplicationContext(),Logined.class);
                        Bundle bs = new Bundle();
                        bs.putSerializable("TaiKhoang", tkCurrent);
                        i.putExtras(bs);
                        startActivity(i);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Tài khoản đã liên kết hoặc mã xác nhận không đúng", Toast.LENGTH_LONG).show();
                        txtstk.setText("");
                        txtst.setText("");
                        txthtct.setText("");
                        txtmxn.setText("");
                        txtnhh.setText("");
                        ram = rd.nextInt((999999 - 100000 + 1) + 100000);
                        String numon = "" + ram;
                        txtcode.setText(numon);
                    }
                }
            }
        });
    }

}
