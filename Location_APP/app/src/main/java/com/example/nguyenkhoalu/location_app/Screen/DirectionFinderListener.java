package com.example.nguyenkhoalu.location_app.Screen;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

//import Modules.Route;
//import com.thaibalong7.demomapapi.Route;
/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
