package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

public class dangky extends Activity {

    Button btndk;
    EditText edtuser, edtpass, edtrepass, edtname, edtcmnd, edtemail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangky);
        btndk = (Button) findViewById(R.id.btnsignup);
        btndk = (Button) findViewById(R.id.btnsignup);
        edtuser = (EditText) findViewById(R.id.edtuserdk);
        edtpass = (EditText) findViewById(R.id.edtpassdk);
        edtrepass = (EditText) findViewById(R.id.edtconpassdk);
        edtname = (EditText) findViewById(R.id.edtnamedk);
        edtcmnd = (EditText) findViewById(R.id.edtcmnddk);
        edtemail = (EditText) findViewById(R.id.edtemaildk);

        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);

        // TẠO BẢNG
        sqLite.query("CREATE TABLE IF NOT EXISTS TAIKHOAN(ID INTEGER PRIMARY KEY AUTOINCREMENT,USER VARCHAR,PASS VARCHAR,HOTEN VARCHAR, CMND VARCHAR,SOTK VARCHAR,EMAIL VARCHAR)");

        btndk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userdk,passdk,repassdk,namedk,cmnddk,emaildk;
                userdk = edtuser.getText().toString();
                passdk = edtpass.getText().toString();
                repassdk = edtrepass.getText().toString();
                namedk = edtname.getText().toString();
                cmnddk = edtcmnd.getText().toString();
                emaildk = edtemail.getText().toString();
                if(userdk.equals("")||passdk.equals("")||repassdk.equals("")||namedk.equals("")||cmnddk.equals("")||emaildk.equals(""))
                    Toast.makeText(getApplicationContext(),"Thông tin chưa được nhập đầy đủ",Toast.LENGTH_LONG).show();
                else
                {
                    String sqlite = String.format("SELECT * FROM TAIKHOAN WHERE USER = '%s'",userdk);
                    Cursor cursor = sqLite.getdata(sqlite);
                    String password = "";
                    while(cursor.moveToNext()) {
                        password = cursor.getString(2);
                    }
                    if(password.equals("")) {
                        if (passdk.equals(repassdk)) {
                            String sql = String.format("INSERT INTO TAIKHOAN VALUES(null,'%s','%s','%s','%s',null,'%s')", userdk, passdk, namedk, cmnddk, emaildk);
                            sqLite.query(sql);
                            Toast.makeText(getApplicationContext(), "Đăng ký tài khoản thành công", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(getApplicationContext(),Login.class);
                            i.putExtra("username",userdk);
                            i.putExtra("password",passdk);
                            startActivity(i);
                        } else {
                            Toast.makeText(getApplicationContext(), "Mật khẩu nhập lại không đúng", Toast.LENGTH_LONG).show();

                            edtpass.setText("");
                            edtrepass.setText("");
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Tài khoản đã tồn tại",Toast.LENGTH_LONG).show();
                        edtuser.setText("");
                        edtpass.setText("");
                        edtrepass.setText("");
                        edtemail.setText("");
                        edtcmnd.setText("");
                    }
                }
            }
        });

    }
}
