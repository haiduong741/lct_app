package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.ImageButton;

import android.widget.TextView;

import com.example.nguyenkhoalu.location_app.R;

public class Logined extends Activity {

    ImageButton btn_NhaHang,btn_CaffeeTraSua,btn_exit;
    TextView txt_NameLogined;
    TAIKHOAN tkCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logined);
        Bundle bR = this.getIntent().getExtras();
        tkCurrent = (TAIKHOAN) bR.getSerializable("TaiKhoang");
        //tkCurrent = (TAIKHOAN) bR.getSerializable("taikhoandadangky");
        connectView(tkCurrent);

    }

    protected void connectView(TAIKHOAN tk)
    {
        txt_NameLogined = (TextView) findViewById(R.id.txt_NameLogined);
        txt_NameLogined.setText(tk.getName());
        btn_NhaHang = (ImageButton) findViewById(R.id.btn_NhaHang);
        btn_NhaHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chuyenActivity(true);

            }
        });
        btn_CaffeeTraSua = (ImageButton) findViewById(R.id.btn_CaffeeTraSua);
        btn_CaffeeTraSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chuyenActivity(false);

            }
        });

        txt_NameLogined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),thongtincanhan.class);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tk);
                i.putExtras(bs);
                startActivity(i);
            }
        });

        btn_exit = (ImageButton) findViewById(R.id.exit);
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Login.class);
                startActivity(i);
            }
        });

    }
    private void chuyenActivity(boolean isNhaHang)
    {
        Intent i = new Intent(getApplicationContext(),findmap.class);
        Log.d("Activity Logined", "Chuyen sang findMap");
        i.putExtra("isNhaHang", isNhaHang);
        startActivity(i);
    }
}
