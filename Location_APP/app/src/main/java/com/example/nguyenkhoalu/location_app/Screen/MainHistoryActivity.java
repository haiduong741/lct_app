package com.example.nguyenkhoalu.location_app.Screen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.nguyenkhoalu.location_app.Adapter.FeedbackAdapter;
import com.example.nguyenkhoalu.location_app.CustomerFeedback.FeedbackInfor;
import com.example.nguyenkhoalu.location_app.DBFeedback.DatabaseFeedback;
import com.example.nguyenkhoalu.location_app.R;

import java.util.ArrayList;
import java.util.List;

public class MainHistoryActivity extends AppCompatActivity {

    private static final int UNG_DUNG = 1;
    private static final int BANH_CANH_ = 2;
    private static final int HU_TIEU = 3;
    private static final int DING_TEA = 4;
    private static final int HOT_AND_COLD = 5;
    private Button btnStoreRating;
    private Button btnStoreHistory;
    private int flag;
    private ListView lvFeedback;
    private DatabaseFeedback DBFeedback;
    private FeedbackAdapter feedbackAdapter;
    private List<FeedbackInfor> listFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_history);

        DBFeedback = new DatabaseFeedback(this);
        listFeedback = new ArrayList<>();
        listFeedback = DBFeedback.getAllFeedback();
        initView();

        btnStoreRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 2;
                Intent intent = new Intent(MainHistoryActivity.this, RatingBarActivity.class);
                intent.putExtra("flag", flag);
                startActivity(intent);
            }
        });

        btnStoreHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdapter();
            }
        });
    }

    public void initView()
    {
        btnStoreRating = (Button) findViewById(R.id.btnChoose2);
        btnStoreHistory = (Button) findViewById(R.id.btnHistory2);
        lvFeedback = (ListView) findViewById(R.id.lvHistory);
    }

    public void setAdapter()
    {
        if(feedbackAdapter == null)
        {
            feedbackAdapter = new FeedbackAdapter(this, R.layout.feedback_information, listFeedback);
            lvFeedback.setAdapter(feedbackAdapter);
        }
        else
        {
            feedbackAdapter.notifyDataSetChanged();
            lvFeedback.setSelection(feedbackAdapter.getCount()-1);
        }
    }
}
