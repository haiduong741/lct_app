package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

public class LienKetBank1 extends Activity {

    ImageButton btnacb, btnagb, btnbidv, btnsacom, btntpb, btnvcb, btnvib, btnvtb, btnvpb;
    TAIKHOAN tkCurrent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lien_ket_bank1);
        btnacb= (ImageButton) findViewById(R.id.btnacb);
        btnagb = (ImageButton) findViewById(R.id.btnagb);
        btnbidv = (ImageButton) findViewById(R.id.btnbidv);
        btnsacom = (ImageButton) findViewById(R.id.btnsacom);
        btntpb = (ImageButton) findViewById(R.id.btntpb);
        btnvcb = (ImageButton) findViewById(R.id.btnvcb);
        btnvib = (ImageButton) findViewById(R.id.btnvib);
        btnvtb = (ImageButton) findViewById(R.id.btnvtb);
        btnvpb = (ImageButton) findViewById(R.id.btnvpb);
        Bundle bR = this.getIntent().getExtras();
        tkCurrent = (TAIKHOAN) bR.getSerializable("TaiKhoang");
        String username = tkCurrent.getUsername();


        btnacb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","ACB");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
        btnagb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","Agribank");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
        btnbidv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","BIDV");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
        btnsacom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","Sacombank");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
        btntpb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","TPbank");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
        btnvcb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","Vietcombank");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
        btnvib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","VIBbank");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
        btnvtb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","Viettinbank");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });

        btnvpb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),lienketbank2.class);
                i.putExtra("bank","VPbank");
                i.putExtra("username",username);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });
    }

}
