package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

import java.util.Random;

public class quenmatkhau extends Activity {

    Button btnsend,btngetcode;
    EditText edtuserqmk, edtpassqmk, edtcmndqmk,edtramdom;
    int ram;
    String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quenmatkhau);

        btnsend = (Button) findViewById(R.id.btnsend);
        btngetcode = (Button)  findViewById(R.id.btngetcode);
        edtuserqmk = (EditText) findViewById(R.id.edtuserqmk);
        edtpassqmk = (EditText) findViewById(R.id.edtpassqmk);
        edtcmndqmk = (EditText) findViewById(R.id.edtcmndqmk);
        edtramdom = (EditText) findViewById(R.id.edtramdom);
        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);
        Random rd=new Random();
        ram =rd.nextInt((999999-99999) + 1 );

        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userqmk,passqmk,cmndqmk,emailqmk;
                userqmk = edtuserqmk.getText().toString();
                passqmk = edtpassqmk.getText().toString();
                cmndqmk = edtcmndqmk.getText().toString();

                if(userqmk.equals("")||passqmk.equals("")||cmndqmk.equals(""))
                    Toast.makeText(getApplicationContext(),"Bạn chưa nhập đầy đủ thông tin",Toast.LENGTH_LONG).show();
                else
                {
                    String sqlite = String.format("SELECT * FROM TAIKHOAN WHERE USER = '%s'",userqmk);
                    Cursor cursor = sqLite.getdata(sqlite);
                    String password = "";
                    String cmnd = "";

                    while(cursor.moveToNext()) {
                        password = cursor.getString(2);
                        cmnd = cursor.getString(4);
                        email = cursor.getString(6);
                    }
                    if(password.equals(""))
                    {
                        Toast.makeText(getApplicationContext(),"Tài khoản không tồn tại",Toast.LENGTH_LONG).show();
                        edtuserqmk.setText("");
                        edtpassqmk.setText("");
                        edtcmndqmk.setText("");
                    }
                    else
                    {
                       String num = edtramdom.getText().toString();
                       int mxn = Integer.parseInt(num);
                       if(mxn == ram)
                       {
                           String sqlup = String.format("UPDATE TAIKHOAN SET PASS = '%s' WHERE USER = '%s'",passqmk,userqmk);
                           sqLite.query(sqlup);
                           Intent i = new Intent(getApplicationContext(),Login.class);
                           i.putExtra("username",userqmk);
                           i.putExtra("password",passqmk);
                           startActivity(i);
                           Toast.makeText(getApplicationContext(),"Xác thực thành công, mật khẩu đã được thay đổi",Toast.LENGTH_LONG).show();
                       }
                       else
                       {
                           edtramdom.setText("");
                           Toast.makeText(getApplicationContext(),"Mã xác nhận không chính xác vui lòng kiểm tra lại",Toast.LENGTH_LONG).show();
                       }
                    }
                }
            }
        });
        btngetcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userqmk = edtuserqmk.getText().toString();
                String sqlite = String.format("SELECT * FROM TAIKHOAN WHERE USER = '%s'",userqmk);
                Cursor cursor = sqLite.getdata(sqlite);

                while(cursor.moveToNext()) {
                    email = cursor.getString(6);
                }
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  ,  new String[] { email });
                Toast.makeText(getApplicationContext(),email,Toast.LENGTH_LONG).show();
                i.putExtra(Intent.EXTRA_SUBJECT, "Xác nhận quên mật khẩu");
                i.putExtra(Intent.EXTRA_TEXT   , "mã xác nhận của bạn: "+ram+" vì lý do bảo mật vui lòng không được tiết lộ cho người khác biết");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
