package com.example.nguyenkhoalu.location_app.CustomerFeedback;

public class FeedbackInfor
{
    private int ID;
    private String nameCustomer;
    private String ratingResult;
    private String emailCustomer;
    private String contentFeedback;
    private String type;
    //private String nameStore;


    public FeedbackInfor(){}

    public FeedbackInfor(String nameCustomer, String ratingResult, String emailCustomer, String contentFeedback)
    {
        // this.nameStore = nameStore;
        this.nameCustomer = nameCustomer;
        this.ratingResult = ratingResult;
        this.emailCustomer = emailCustomer;
        this.contentFeedback = contentFeedback;
        //this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameCustomer() {
        return nameCustomer;

    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getRatingResult() {
        return ratingResult;
    }

    public void setRatingResult(String ratingResult) {
        this.ratingResult = ratingResult;
    }

    public String getEmailCustomer() {
        return emailCustomer;
    }

    public void setEmailCustomer(String emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    public String getContentFeedback() {
        return contentFeedback;
    }

    public void setContentFeedback(String contentFeedback) {
        this.contentFeedback = contentFeedback;
    }
}
