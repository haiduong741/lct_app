package com.example.nguyenkhoalu.location_app.DBFeedback;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.nguyenkhoalu.location_app.CustomerFeedback.FeedbackInfor;

import java.util.ArrayList;
import java.util.List;

public class DatabaseFeedback extends SQLiteOpenHelper
{
    private final String TAG = "DBFeedBack";
    private static final String DATABASE_NAME = "Feedback_Management";
    private static final String TABLE_NAME = "Phan_Hoi";
    private static final String NAME = "Tên";
    private static final String EMAIL = "Email";
    private static final String CONTENT = "Noi_Dung";
    private static final String RATE = "So_Sao";
    private static final String ID = "id";
    private static final String TYPE = "Noi_dung";
    private static int VERSION = 1;

    private Context context;
    private String SQLQuery = "CREATE TABLE " + TABLE_NAME + " (" +
            ID + " integer primary key, " +
            //NAME_STORE + " TEXT, "+
            NAME + " TEXT, " +
            EMAIL + " TEXT, " +
            RATE + " TEXT, " +
            CONTENT + " TEXT)";

    public DatabaseFeedback(Context context)
    {
        super(context, DATABASE_NAME, null, VERSION);
        this.context = context;
        Log.d(TAG, "DBFeedback: ");
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(SQLQuery);
        Log.d(TAG, "OnCreate: ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        Log.d(TAG, "OnUpgrade: ");
    }

    public void addFeedback(FeedbackInfor feedback)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME, feedback.getNameCustomer());
        values.put(EMAIL, feedback.getEmailCustomer());
        values.put(RATE, feedback.getRatingResult());
        values.put(CONTENT, feedback.getContentFeedback());

        db.insert(TABLE_NAME, null, values);
        db.close();
        Log.d(TAG, "addFeedback Successfully..!!");
    }

    public List<FeedbackInfor> getAllFeedback()
    {
        List<FeedbackInfor> listFeedback = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst())
        {
            do {
                FeedbackInfor feedback = new FeedbackInfor();

                feedback.setID(cursor.getInt(0));
                feedback.setNameCustomer(cursor.getString(1));
                feedback.setEmailCustomer(cursor.getString(2));
                feedback.setRatingResult(cursor.getString(3));
                feedback.setContentFeedback(cursor.getString(4));

                listFeedback.add(feedback);
            }while (cursor.moveToNext());
        }

        db.close();
        return listFeedback;
    }

    public void deleteFeedback(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, ID +"=?",new String[] {String.valueOf(id)});
    }
}
