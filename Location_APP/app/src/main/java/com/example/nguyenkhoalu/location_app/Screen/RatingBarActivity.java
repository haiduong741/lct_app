package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.Adapter.FeedbackAdapter;
import com.example.nguyenkhoalu.location_app.CustomerFeedback.FeedbackInfor;
import com.example.nguyenkhoalu.location_app.DBFeedback.DatabaseFeedback;
import com.example.nguyenkhoalu.location_app.R;

import java.util.List;

public class RatingBarActivity extends Activity {

    private Button btnSend;
    private Button btnBack;
    private RatingBar rtBar;
    private EditText edtRate;
    private EditText edtNameCustomer;
    private EditText edtEmail;
    private EditText edtContent;
    private TextView tvFlag;
    private DatabaseFeedback DBFeedback;
    private FeedbackAdapter feedbackAdapter;
    private List<FeedbackInfor> listFeedback;

    int i = 0;
    float tem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_bar);

        DBFeedback = new DatabaseFeedback(this);
        initView();
        listFeedback = DBFeedback.getAllFeedback();

        rtBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                edtRate.setText("Rate: "+ String.valueOf(rating));
                tem = rating;
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedbackInfor feedbackInfor = createListFeedback();
                if(feedbackInfor != null)
                {
                    DBFeedback.addFeedback(feedbackInfor);
                }
                updateListFeedback();
                Toast.makeText(RatingBarActivity.this, "Cảm ơn bạn đã đánh giá!", Toast.LENGTH_SHORT).show();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RatingBarActivity.this, chitietquan.class);
                startActivity(intent);

            }
        });
    }

    public void initView()
    {
        rtBar = (RatingBar) findViewById(R.id.rtBar1);
        edtRate = (EditText) findViewById(R.id.edtRate);
        edtNameCustomer = (EditText) findViewById(R.id.edtNameCustomer);
        edtEmail = (EditText) findViewById(R.id.edtEmailCustomer);
        edtContent = (EditText) findViewById(R.id.edtContent);
        tvFlag = (TextView) findViewById(R.id.tvFlag);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnBack = (Button) findViewById(R.id.btnBack);

        Intent i = getIntent();
        int flag = i.getIntExtra("flag", 1);

        if(flag == 1)
        {
            tvFlag.setText("ĐÁNH GIÁ ỨNG DỤNG: ");
        }
        else
        {
            tvFlag.setText("ĐÁNH GIÁ CỬA HÀNG: ");
        }
    }

    public FeedbackInfor createListFeedback()
    {
        String nameCustomer = edtNameCustomer.getText().toString();
        String email = "Email: " + edtEmail.getText().toString();
        String content = edtContent.getText().toString();
        String rate = edtRate.getText().toString();

        Log.d("CreateList", "");
        Log.d("Name", nameCustomer);
        Log.d("email", email);
        Log.d("content", content);
        Log.d("rate", rate);

        if(tem >= 4.0)
        {
            rate = "TUYỆT VỜI";
        }
        else if(tem < 4.0 && tem >= 3.0)
        {
            rate = "TỐT";
        }
        else if(tem < 3.0 && tem >= 2.0)
        {
            rate = "BÌNH THƯỜNG";
        }
        else if(tem < 2.0 && tem >= 1.0)
        {
            rate = "KHÁ TỆ";
        }
        else{
            rate = "RẤT TỆ";
        }

        rate = "Đánh giá: " + rate;
        String type = "ỨNG DỤNG";

        FeedbackInfor feedbackInfor = new FeedbackInfor(nameCustomer, rate, email, content);
        return feedbackInfor;
    }

    public void updateListFeedback()
    {
        listFeedback.clear();
        listFeedback.addAll(DBFeedback.getAllFeedback());
        if(feedbackAdapter != null)
        {
            feedbackAdapter.notifyDataSetChanged();
        }
    }
}
