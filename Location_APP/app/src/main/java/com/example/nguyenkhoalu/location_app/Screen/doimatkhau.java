package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

public class doimatkhau extends Activity {

    Button btnchange;
    EditText edtuserdmk, edtpassolddmk, edtcmnddmk, edtpassnewdmk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doimatkhau);

        btnchange = (Button) findViewById(R.id.btnchange);
        edtuserdmk = (EditText) findViewById(R.id.edtuserdmk);
        edtcmnddmk = (EditText) findViewById(R.id.edtcmnddmk);
        edtpassolddmk = (EditText) findViewById(R.id.edtpassolddmk);
        edtpassnewdmk = (EditText) findViewById(R.id.edtpassnewdmk);

        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);

        btnchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userdmk,passolddmk,cmnddmk,passnewqmk;
                userdmk = edtuserdmk.getText().toString();
                passolddmk = edtpassolddmk.getText().toString();
                cmnddmk = edtcmnddmk.getText().toString();
                passnewqmk = edtpassnewdmk.getText().toString();

                if(userdmk.equals("")||passnewqmk.equals("")||cmnddmk.equals("")||passolddmk.equals(""))
                    Toast.makeText(getApplicationContext(),"Bạn chưa nhập đầy đủ thông tin",Toast.LENGTH_LONG).show();
                else
                {
                    String sqlite = String.format("SELECT * FROM TAIKHOAN WHERE USER = '%s'",userdmk);
                    Cursor cursor = sqLite.getdata(sqlite);
                    String password = "";
                    String cmnd = "";
                    while(cursor.moveToNext()) {
                        password = cursor.getString(2);
                        cmnd = cursor.getString(4);
                    }
                    if(password.equals(""))
                    {
                        Toast.makeText(getApplicationContext(),"Tài khoản không tồn tại",Toast.LENGTH_LONG).show();
                        edtuserdmk.setText("");
                        edtpassolddmk.setText("");
                        edtpassnewdmk.setText("");
                        edtcmnddmk.setText("");
                    }
                    else
                    {
                        if(passolddmk.equals(password) && cmnddmk.equals(cmnd))
                        {
                            String sqlup = String.format("UPDATE TAIKHOAN SET PASS = '%s' WHERE USER = '%s'",passnewqmk,userdmk);
                            sqLite.query(sqlup);
                            Toast.makeText(getApplicationContext(),"Đổi mật khẩu thành công",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Mật khẩu cũ hoặc CMND không đúng",Toast.LENGTH_LONG).show();
                            edtcmnddmk.setText("");
                            edtpassolddmk.setText("");
                            edtpassnewdmk.setText("");
                        }
                    }
                }
            }
        });
    }
}
