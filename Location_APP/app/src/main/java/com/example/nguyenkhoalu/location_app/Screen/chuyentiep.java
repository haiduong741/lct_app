package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.nguyenkhoalu.location_app.R;

public class chuyentiep extends Activity {

    private String array_spinner[];
    Button btncon;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chuyentiep);

        btncon = (Button) findViewById(R.id.btncon);
        spinner = (Spinner) findViewById(R.id.spinner);

        array_spinner=new String[2];
        array_spinner[0]="Khách hàng";
        array_spinner[1]="Nhà cung cấp";

        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, array_spinner);
        spinner.setAdapter(adapter);

        btncon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = (String)spinner.getSelectedItem();
                if(ss.equals(array_spinner[0]))
                {
                    Intent i = new Intent(getApplicationContext(),dangky.class);
                    startActivity(i);
                }
                else
                {
                    Intent i = new Intent(getApplicationContext(),dangkidichvu.class);
                    startActivity(i);
                }
            }
        });
    }
}
