package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

public class doithongtin extends Activity {

    Button btnchangedtt;
    EditText edtnamedtt,edtcmndtt,edtstkdtt;
    TAIKHOAN tkCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doithongtin);

        btnchangedtt = (Button) findViewById(R.id.btnchangedtt);
        edtnamedtt = (EditText) findViewById(R.id.edtnamedtt);
        edtcmndtt = (EditText) findViewById(R.id.edtcmndtt);
        edtstkdtt = (EditText) findViewById(R.id.edtstkdtt);
        Bundle bR = this.getIntent().getExtras();
        tkCurrent = (TAIKHOAN) bR.getSerializable("TaiKhoang");

        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);

        btnchangedtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String namedtt,cmndtt,stkdtt;
                namedtt = edtnamedtt.getText().toString();
                cmndtt = edtcmndtt.getText().toString();
                stkdtt = edtstkdtt.getText().toString();
                String user = tkCurrent.getUsername();

                if(namedtt.equals("")||cmndtt.equals("")||stkdtt.equals(""))
                    Toast.makeText(getApplicationContext(),"Bạn chưa nhập đầy đủ thông tin",Toast.LENGTH_LONG).show();
                else
                {
                    String sqlup = String.format("UPDATE TAIKHOAN SET HOTEN = '%s', CMND = '%s', SOTK = '%s' WHERE USER = '%s'",namedtt,cmndtt,stkdtt,user);
                    sqLite.query(sqlup);
                    Toast.makeText(getApplicationContext(),"Thay đổi thông tin thành công",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(),Logined.class);
                    tkCurrent.setName(namedtt);
                    tkCurrent.setCMND(cmndtt);
                    tkCurrent.setNumofAcount(stkdtt);
                    Bundle bs = new Bundle();
                    bs.putSerializable("TaiKhoang", tkCurrent);
                    i.putExtras(bs);
                    startActivity(i);
                }
            }
        });
    }
}
