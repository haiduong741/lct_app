package com.example.nguyenkhoalu.location_app.Screen;

import java.io.Serializable;

/**
 * Created by thaib on 2018-05-07.
 */

public class marker implements Serializable {
    int id;
    String name;
    String sodt;
    String sotk;
    String diachi;
    String loaihinh;
    String kinhdo;
    String vido;

    public marker() {
    }

    public marker(int id, String name, String sodt, String sotk, String diachi, String loaihinh, String kinhdo, String vido) {
        this.id = id;
        this.name = name;
        this.sodt = sodt;
        this.sotk = sotk;
        this.diachi = diachi;
        this.loaihinh = loaihinh;
        this.kinhdo = kinhdo;
        this.vido = vido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSodt() {
        return sodt;
    }

    public void setSodt(String sodt) {
        this.sodt = sodt;
    }

    public String getSotk() {
        return sotk;
    }

    public void setSotk(String sotk) {
        this.sotk = sotk;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getLoaihinh() {
        return loaihinh;
    }

    public void setLoaihinh(String loaihinh) {
        this.loaihinh = loaihinh;
    }

    public String getKinhdo() {
        return kinhdo;
    }

    public void setKinhdo(String kinhdo) {
        this.kinhdo = kinhdo;
    }

    public String getVido() {
        return vido;
    }

    public void setVido(String vido) {
        this.vido = vido;
    }

    @Override
    public String toString() {
        return "marker{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sodt='" + sodt + '\'' +
                ", sotk='" + sotk + '\'' +
                ", diachi='" + diachi + '\'' +
                ", loaihinh='" + loaihinh + '\'' +
                ", kinhdo='" + kinhdo + '\'' +
                ", vido='" + vido + '\'' +
                '}';
    }
}
