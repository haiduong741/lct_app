//package com.example.nguyenkhoalu.location_app.Screen;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.database.Cursor;
//import android.os.Parcelable;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.example.nguyenkhoalu.location_app.R;
//
//import java.io.Serializable;
//
//public class Login extends Activity {
//
//    Button btnpress;
//    EditText user, pass;
//    TextView txtdk, txtquen,txtdoi;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);
//
//        btnpress = (Button) findViewById(R.id.button);
//        user = (EditText) findViewById(R.id.etduser);
//        pass = (EditText) findViewById(R.id.etdpass);
//        txtdk = (TextView) findViewById(R.id.txtsingup);
//        txtdoi = (TextView) findViewById(R.id.txtdoimatkhau);
//        txtquen = (TextView) findViewById(R.id.txtquen);
//
//        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);
//
//        sqLite.query("CREATE TABLE IF NOT EXISTS TAIKHOAN(ID INTEGER PRIMARY KEY AUTOINCREMENT,USER VARCHAR,PASS VARCHAR,HOTEN VARCHAR, CMND VARCHAR,SOTK VARCHAR,EMAIL VARCHAR)");
//
//
//
//        btnpress.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(user.getText().toString().equals("") || pass.getText().toString().equals(""))
//                    Toast.makeText(getApplicationContext(),"Tài khoản, mật khẩu không được để trống",Toast.LENGTH_LONG).show();
//                else
//                {
//                    String username = user.getText().toString();
//                    String sql = String.format("SELECT * FROM TAIKHOAN WHERE USER = '%s'",username);
//                    Cursor cursor = sqLite.getdata(sql);
//                    int matk;
//                    String password = "";
//                    String usertk = "";
//                    String nametk = "";
//                    String CMNDtk = "";
//                    String NumofAcount = "";
//                    String Emailtk = "";
//                    TAIKHOAN tk = null;
//                    while(cursor.moveToNext()) {
//                        matk = cursor.getInt(0);
//                        usertk = cursor.getString(1);
//                        password = cursor.getString(2);
//                        nametk = cursor.getString(3);
//                        CMNDtk = cursor.getString(4);
//                        NumofAcount = cursor.getString(5);
//                        Emailtk = cursor.getString(6);
//                        tk = new TAIKHOAN(matk,usertk,password,nametk,CMNDtk,NumofAcount,Emailtk);
//                    }
//                    if(password.equals("")) {
//                        Toast.makeText(getApplicationContext(), "Tài khoản không tồn tại", Toast.LENGTH_LONG).show();
//                        user.setText("");
//                        pass.setText("");
//                    }
//                    else
//                    {
//                        if(password.equals(pass.getText().toString())) {
//                            Toast.makeText(getApplicationContext(), "Đăng nhập thành công", Toast.LENGTH_LONG).show();
//                            Intent i = new Intent(getApplicationContext(),Logined.class);
//                            Bundle bs = new Bundle();
//                            bs.putSerializable("TaiKhoang", tk);
//                            i.putExtras(bs);
//                            startActivity(i);
//                        }
//                        else {
//                            Toast.makeText(getApplicationContext(), "Sai mật khẩu", Toast.LENGTH_LONG).show();
//                            pass.setText("");
//                        }
//                    }
//                }
//            }
//        });
//        txtdk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(getApplicationContext(),chuyentiep.class);
//                startActivity(i);
//
//            }
//        });
//
//        txtquen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(getApplicationContext(),quenmatkhau.class);
//                startActivity(i);
//            }
//        });
//
//        txtdoi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(getApplicationContext(),doimatkhau.class);
//                startActivity(i);
//            }
//        });
//    }
//}
package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

import java.io.Serializable;

public class Login extends Activity {

    Button btnpress;
    EditText user, pass;
    TextView txtdk, txtquen,txtdoi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnpress = (Button) findViewById(R.id.button);
        user = (EditText) findViewById(R.id.etduser);
        pass = (EditText) findViewById(R.id.etdpass);
        txtdk = (TextView) findViewById(R.id.txtsingup);
        txtdoi = (TextView) findViewById(R.id.txtdoimatkhau);
        txtquen = (TextView) findViewById(R.id.txtquen);

        Intent i = getIntent();
        String uss = i.getStringExtra("username");
        String passss = i.getStringExtra("password");
        user.setText(uss);
        pass.setText(passss);

        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);

        sqLite.query("CREATE TABLE IF NOT EXISTS TAIKHOAN(ID INTEGER PRIMARY KEY AUTOINCREMENT,USER VARCHAR,PASS VARCHAR,HOTEN VARCHAR, CMND VARCHAR,SOTK VARCHAR,EMAIL VARCHAR)");

        sqLite.query("CREATE TABLE IF NOT EXISTS DANGKYDICHVU(ID INTEGER PRIMARY KEY AUTOINCREMENT,TENCUAHANG VARCHAR(100),SODT VARCHAR,STK VARCHAR,DIACHI VARCHAR(300),LOAIHINH VARCHAR,KINHDO VARCHAR,VIDO VARCHAR)");

         sqLite.query("INSERT INTO TAIKHOAN VALUES(null,'lehuuly','123','Lê Hữu Lý','321542964','2342342342','lehuuly300796@gmail.com')");

        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Bánh canh Ghẹ Muối Ớt Xanh','+84 91 650 19 10','123456789','484 Nguyễn Tri Phương, Phường 9, Quận 10, Hồ Chí Minh, Việt Nam','Nhà hàng','10.767292','106.667399')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Trà sữa Alley','+84 91 650 19 11','123456788','40 Phạm Ngọc Thái, Phường 4, Quận 2, Hồ Chí Minh, Việt Nam','Caffee - Trà sữa','10.764142','106.682217')");

        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Domino''s Pizza','+84 1900 6099','123456288','313 Nguyễn Tri Phương, Phường 5, Quận 10, Hồ Chí Minh, Việt Nam','Nhà hàng','10.761896','106.668193')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Effoc Coffee','+84 28 6299 7361','1235476288','499 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí Minh, Việt Nam','Caffee - Trà sữa','10.774187','106.668254')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Ganesh Indian Restaurant','+84 28 5410 1627','129456288','So pmh, 54 Đường số 6, Tân Phong, Quận 7, Hồ Chí Minh, Việt Nam','Nhà hàng','10.730366','106.707668')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Here & Now Vegetarian','+84 28 6292 6669','123455488','89 E Nguyễn Công Hoan, Phường 7, Phú Nhuận, Hồ Chí Minh, Việt Nam','Nhà hàng','10.800055','106.690548')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Thiên Phúc cafe','+84 90 919 00 18','134456288','15 Nguyễn Văn Đừng, Phường 6, Quận 5, Hồ Chí Minh, Việt Nam','Caffee - Trà sữa','10.752133','106.671261')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Tokyo Deli','+84 28 6297 2277','136456288','386 - 398 Đường 3/2, Phường 12, Quận 10, phường 12 Quận 10 Hồ Chí Minh, Việt Nam','Nhà hàng','10.769537','106.669759')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Trà Sữa Bệt 89+','+84 94 623 23 69','134456488','P., 235B Đường Nguyễn Văn Cừ, Phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh, Việt Nam','Caffee - Trà sữa','10.762860','106.683006')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Trà sữa Hoa Hướng Dương','+84 28 6271 1990','134456288','235B Đường Nguyễn Văn Cừ, Phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh, Việt Nam','Caffee - Trà sữa','10.761770','106.683224')");
        sqLite.query("INSERT INTO DANGKYDICHVU VALUES(null,'Xúc Xích Đức Vỉa Hè - 174 Dương Bá Trạc . P.2 .Q.8','+84 93 721 67 70','134456288','174 Dương Bá Trạc, Phường 2, Quận 8, Hồ Chí Minh, Việt Nam','Nhà hàng','10.747157','106.688773')");




        btnpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user.getText().toString().equals("") || pass.getText().toString().equals(""))
                    Toast.makeText(getApplicationContext(),"Tài khoản, mật khẩu không được để trống",Toast.LENGTH_LONG).show();
                else
                {
                    String username = user.getText().toString();
                    String sql = String.format("SELECT * FROM TAIKHOAN WHERE USER = '%s'",username);
                    Cursor cursor = sqLite.getdata(sql);
                    int matk;
                    String password = "";
                    String usertk = "";
                    String nametk = "";
                    String CMNDtk = "";
                    String NumofAcount = "";
                    String Emailtk = "";
                    TAIKHOAN tk = null;
                    while(cursor.moveToNext()) {
                        matk = cursor.getInt(0);
                        usertk = cursor.getString(1);
                        password = cursor.getString(2);
                        nametk = cursor.getString(3);
                        CMNDtk = cursor.getString(4);
                        NumofAcount = cursor.getString(5);
                        Emailtk = cursor.getString(6);
                        tk = new TAIKHOAN(matk,usertk,password,nametk,CMNDtk,NumofAcount,Emailtk);
                    }
                    if(password.equals("")) {
                        Toast.makeText(getApplicationContext(), "Tài khoản không tồn tại", Toast.LENGTH_LONG).show();
                        user.setText("");
                        pass.setText("");
                    }
                    else
                    {
                        if(password.equals(pass.getText().toString())) {
                            Toast.makeText(getApplicationContext(), "Đăng nhập thành công", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(getApplicationContext(),Logined.class);
                            Bundle bs = new Bundle();
                            bs.putSerializable("TaiKhoang", tk);
                            i.putExtras(bs);
                            startActivity(i);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Sai mật khẩu", Toast.LENGTH_LONG).show();
                            pass.setText("");
                        }
                    }
                }
            }
        });
        txtdk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),chuyentiep.class);
                startActivity(i);

            }
        });

        txtquen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),quenmatkhau.class);
                startActivity(i);
            }
        });

        txtdoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),doimatkhau.class);
                startActivity(i);
            }
        });
    }
}
