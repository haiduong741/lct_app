package com.example.nguyenkhoalu.location_app.Screen;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class findmap extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        DirectionFinderListener,
        GoogleMap.OnMarkerClickListener {


    GoogleMap map;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String TAG = "findmap";
    private static final float DEFAULT_ZOOM = 16f;
    private LatLng curLatLng;
    private LatLng curChoosenLatLng = null;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    ImageView btn_ChiDuong;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    private boolean isNhaHang;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_findmap);
        Intent i = getIntent();
        isNhaHang = i.getBooleanExtra("isNhaHang", true);
        Log.d(TAG, "onCreate");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.myMap);
        mapFragment.getMapAsync(this);


        btn_ChiDuong = (ImageView) findViewById(R.id.ChiDuong);
        btn_ChiDuong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //LatLng BenThanh = new LatLng(10.771343, 106.694289);
                if (curChoosenLatLng!=null) {
                    getCurrentDeviceLocation(false);
                    Log.d(TAG, "Start sendRequest, Direction to " +curChoosenLatLng.toString());

                    sendRequest(curChoosenLatLng);
                }
                else { makeToast("Chưa chọn điểm đến");}
            }
        });

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    public void makeToast(String a)
    {
        Toast.makeText(this, a, Toast.LENGTH_LONG).show();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;


        boolean success = map.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.style_json)));

        if (!success) {
            Log.e(TAG, "Style parsing failed.");
        }

        checkLocationPermission();
        map.setOnMyLocationButtonClickListener(this);
        map.setOnMyLocationClickListener(this);
        map.setOnMarkerClickListener(this);
        map.getUiSettings().setMapToolbarEnabled(false);
        ganListMarker(map);


    }


    public boolean checkLocationPermission() //kiểm tra ứng dụng đã cấp quyền truy cập GPS chưa
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //nếu thiết bị đã cho phép truy cập thông tin vị trí
            getCurrentDeviceLocation(true);
            map.setMyLocationEnabled(true);
            return true;
        } else {
            // Should we show an explanation?
            //ngược lại là chưa ==> tạo requestPermission để hỏi có đồng ý hay không
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(this)
                        .setTitle("Location permission")
                        .setMessage("Ứng dụng cần bạn cấp quyền chia sẽ vị trí GPS")
                        .setPositiveButton("ĐỒNG Ý", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(findmap.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_LOCATION is an
                // app-defined int constant. The callback method gets the
                // result of the request.

                //map.setMyLocationEnabled(true);

            }
            return false;
            //return;
            // Show rationale and request permission.
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Nếu người dùng nhấp vào cho phép ở cái Request hiện lên
                    getCurrentDeviceLocation(true);

                    map.setMyLocationEnabled(true);
                    // permission was granted, yay! Do the
                    // location-related task you need to do.

                } else {
                    //ngược lại
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        //khi nhấn vào nút xanh (vị trí hiện tại)
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
        //Toast.makeText(this, "Current location by func:\n" + getCurrentLocation(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        //khi nhấn vào nút cập nhật vị trí
        getCurrentDeviceLocation(false);
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    private void getCurrentDeviceLocation(final boolean isMoveCamera) {
        Log.d(TAG, "Getting the devices current location");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            mFusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            Log.d(TAG, "onComplete: found location" +location);
//                            Location currentLocation = (Location) task.getResult();
                            //curLocation = (Location) task.getResult();
                            curLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                            if (isMoveCamera == true)
                            {
                                moveCamera(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM);
                            }
                            if (location == null) {
                                // Logic to handle location object
                                Log.d(TAG, "onComplete: current location is null");
                            }
                        }
                    });
//            mFusedLocationProviderClient.getLastLocation().getResult();

        } catch (SecurityException e) {
            Log.e(TAG, e + "");
        }


    }

    private void moveCamera(LatLng latLng, float zoom) {

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", Lng" + latLng.longitude);
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));

//            //Vẽ 2 cái maker ở 2 điểm đầu và đích
//            originMarkers.add(map.addMarker(new MarkerOptions()
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
//                    .title(route.startAddress)
//                    .position(route.startLocation)));
            destinationMarkers.add(map.addMarker(new MarkerOptions()
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker01))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(map.addPolyline(polylineOptions));
        }
    }
    private void sendRequest(LatLng originAddress, LatLng destinationAddress) {
        String origin = originAddress.latitude+",%20"+originAddress.longitude;
        String destination = destinationAddress.latitude+",%20"+destinationAddress.longitude;

        Log.d(TAG, "sendRequest");

        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    private void sendRequest(LatLng destinationAddress) {
        String destination = destinationAddress.latitude+",%20"+destinationAddress.longitude;
        getCurrentDeviceLocation(false);
        LatLng originAddress = curLatLng;
        String origin = originAddress.latitude+",%20"+originAddress.longitude;
//        if (origin.isEmpty()) {
//            Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if (destination.isEmpty()) {
//            Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
//            return;
//        }
        Log.d(TAG, "sendRequest");

        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onMarkerClick(final Marker marker) {
        curChoosenLatLng =  marker.getPosition();
        com.example.nguyenkhoalu.location_app.Screen.marker k = (com.example.nguyenkhoalu.location_app.Screen.marker) marker.getTag();
        Log.d(TAG,"Click Marker: getTag"+k.toString());
        Log.d(TAG, "Click Marker: getLatLng"+curChoosenLatLng.toString());
        Intent i = new Intent(getApplicationContext(),chitietquan.class);
        Bundle bs = new Bundle();
        bs.putSerializable("Marker", k);
        i.putExtras(bs);
        startActivity(i);
        return false;
    }

        public ArrayList<marker> getDataMarker()
    {
        //Lấy dữ liệu là các cửa hàng
        Log.d("findmap", "getData");
        ArrayList<marker> listmarker = new ArrayList<>();
        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);

        String sql = "SELECT * FROM DANGKYDICHVU";
        Cursor cursor = sqLite.getdata(sql);
        int id = 0;
        String name = "";
        String sodt = "";
        String sotk = "";
        String diachi = "";
        String loaihinh = "";
        String kinhdo = "";
        String vido = "";
        while(cursor.moveToNext())
        {
            id = cursor.getInt(0);
            name = cursor.getString(1);
            sodt = cursor.getString(2);
            sotk = cursor.getString(3);
            diachi = cursor.getString(4);
            loaihinh = cursor.getString(5);
            kinhdo  = cursor.getString(6);
            vido = cursor.getString(7);
            marker mark = new marker(id,name,sodt,sotk,diachi,loaihinh,kinhdo,vido);
            listmarker.add(mark);
            Log.d("findmap", "getData: "+mark.toString());
        }
        return listmarker;
    }

    private void ganListMarker(GoogleMap m)
    {
        Log.d(TAG,"Gan Marker: isNhaHang "+isNhaHang);
        ArrayList<marker> dataMarker = getDataMarker();
        if (isNhaHang == true) {

            for (int i = 0; i < dataMarker.size(); i++) {
                if (dataMarker.get(i).getLoaihinh().equals("Nhà hàng")) {
                    ganMarker(dataMarker.get(i), m, true);
                }
            }
        }
        else {
            for (int i = 0; i < dataMarker.size(); i++) {
                if (!dataMarker.get(i).getLoaihinh().equals("Nhà hàng")) {
                    ganMarker(dataMarker.get(i), m, false);
                }
            }
        }
    }

    private void ganMarker(marker m, GoogleMap ma, boolean isNhaHang)
    {
        BitmapDescriptor icon;
        if (isNhaHang == true){ icon = BitmapDescriptorFactory.fromResource(R.drawable.maker_food);}
        else {icon = BitmapDescriptorFactory.fromResource(R.drawable.maker_water);}
        Log.d("Them Marker",m.toString());
        float Lat = Float.parseFloat(m.getKinhdo());
        float Lng = Float.parseFloat(m.getVido());
        LatLng LL = new LatLng(Lat, Lng);
        //markerTemp.
        Marker maker = ma.addMarker(new MarkerOptions().title(m.getName())
                                        .position(LL)
                                        .icon(icon)
                                        );
        maker.setTag(m);
    }


}
