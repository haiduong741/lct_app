package com.example.nguyenkhoalu.location_app.CustomViewss;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nguyenkhoalu.location_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by macbookpro on 11/18/17.
 */

public class CustomToolBarPrev extends RelativeLayout {

    @BindView(R.id.rlLeftButton)
    View rlLeftButton;
    @BindView(R.id.ivLeft)
    ImageView ivLeft;

    @BindView(R.id.rlLeftText)
    View rlLeftText;
    @BindView(R.id.tvLeft)
    TextView tvLeft;

    @BindView(R.id.rlRightButton)
    View rlRightButton;
    @BindView(R.id.ivRight)
    ImageView ivRight;


    @BindView(R.id.rlRightText)
    View rlRightText;
    @BindView(R.id.tvRight)
    TextView tvRight;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSubTitle)
    TextView tvSubTitle;

    private View viewRoot;
    private OnToolbarClickListener listener;


    public CustomToolBarPrev(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewRoot = inflater.inflate(R.layout.custom_toolbar, this, true);
        ButterKnife.bind(viewRoot);

        initStyle(attrs);
    }

    public void showLeftButton() {
        rlLeftButton.setVisibility(VISIBLE);
        if (rlLeftText.getVisibility() == VISIBLE) {
            rlLeftText.setVisibility(GONE);
        }
    }

    public void hideLeftButton() {
        rlLeftButton.setVisibility(INVISIBLE);
    }


    public void showLeftText() {
        rlLeftText.setVisibility(VISIBLE);
        if (rlLeftButton.getVisibility() == VISIBLE) {
            rlLeftButton.setVisibility(INVISIBLE);
        }
    }


    public void hideLeftText() {
        rlLeftText.setVisibility(GONE);
    }

    public void showRightButton() {
        rlRightButton.setVisibility(VISIBLE);
        if (rlRightText.getVisibility() == VISIBLE) {
            rlRightText.setVisibility(GONE);
        }
    }

    public void hideRightButton() {
        rlRightButton.setVisibility(INVISIBLE);
    }

    public void showRightText() {
        rlRightText.setVisibility(VISIBLE);
        if (rlRightButton.getVisibility() == VISIBLE) {
            rlRightButton.setVisibility(INVISIBLE);
        }
    }

    public void hideRightText() {
        rlRightText.setVisibility(GONE);
    }

    public void setRlRightText(String text){
        tvRight.setText(text);
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public void setTitleColor(int color) {
        tvTitle.setTextColor(color);
    }

    public void setTvSubTitle(String subTitle) {
        tvSubTitle.setText(subTitle);
    }

    public void showSubTitle() {
        tvSubTitle.setVisibility(VISIBLE);
    }

    public void hideSubTitle() {
        tvSubTitle.setVisibility(GONE);
    }

    public void setLeftIcon(int id) {
        ivLeft.setImageResource(id);
    }

    public void setRightIcon(int id) {
        ivRight.setImageResource(id);
    }

    public void setToolbarClickListener(OnToolbarClickListener listener) {
        this.listener = listener;

        rlLeftButton.setOnClickListener(v -> listener.onLeftButtonClick());
        rlLeftText.setOnClickListener(v -> listener.onLeftButtonClick());

        rlRightButton.setOnClickListener(v -> listener.onRightButtonClick());
        rlRightText.setOnClickListener(v -> listener.onRightButtonClick());
    }

    public void setRlLeftText(String rlLeftText) {
        tvLeft.setText(rlLeftText);
    }

    public interface OnToolbarClickListener {
        void onLeftButtonClick();

        void onRightButtonClick();
    }

    private void initStyle(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomToolBarPrev);
        tvTitle.setText(typedArray.getString(R.styleable.CustomToolBarPrev_toolBarTitle));
        // image view right
        ivRight.setVisibility(typedArray.getBoolean(R.styleable.CustomToolBarPrev_showEndIcon, false) ? VISIBLE : INVISIBLE);
        ivRight.setImageDrawable(typedArray.getDrawable(R.styleable.CustomToolBarPrev_endIcon));
        // image vew left
        ivLeft.setVisibility(typedArray.getBoolean(R.styleable.CustomToolBarPrev_showStartIcon, false) ? VISIBLE : INVISIBLE);
        ivLeft.setImageDrawable(typedArray.getDrawable(R.styleable.CustomToolBarPrev_startIcon));
        // Text view Right
        rlRightText.setVisibility(typedArray.getBoolean(R.styleable.CustomToolBarPrev_showEndText, false) ? VISIBLE : INVISIBLE);
        tvRight.setText(typedArray.getString(R.styleable.CustomToolBarPrev_endText));
        // Text view Left
        rlLeftText.setVisibility(typedArray.getBoolean(R.styleable.CustomToolBarPrev_showStartText, false) ? VISIBLE : INVISIBLE);
        tvLeft.setText(typedArray.getString(R.styleable.CustomToolBarPrev_startText));
        typedArray.recycle();
    }
}
