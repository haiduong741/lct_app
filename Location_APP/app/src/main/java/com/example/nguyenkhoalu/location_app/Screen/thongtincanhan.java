package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

import org.w3c.dom.Text;

public class thongtincanhan extends Activity {

    Button btnedit;
    TAIKHOAN tkCurrent;
    TextView txtlienket_nganhang,txtname,txtcmnd,txtemail,txtstk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thongtincanhan);
        Bundle bR = this.getIntent().getExtras();
        tkCurrent = (TAIKHOAN) bR.getSerializable("TaiKhoang");
        btnedit = (Button) findViewById(R.id.btnedit);
        txtname = (TextView) findViewById(R.id.txtname);
        txtcmnd = (TextView) findViewById(R.id.txtcmndttcn);
        txtemail = (TextView) findViewById(R.id.txtemailttcn);
        txtstk = (TextView) findViewById(R.id.txtstkttcn);
        txtlienket_nganhang = (TextView) findViewById(R.id.txtlienket) ;


        txtname.setText(tkCurrent.getName());
        txtcmnd.setText(tkCurrent.getCMND());
        txtemail.setText(tkCurrent.getEmail());
        txtstk.setText(tkCurrent.getNumofAcount());

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),doithongtin.class);
                Bundle bs = new Bundle();
                bs.putSerializable("TaiKhoang", tkCurrent);
                i.putExtras(bs);
                startActivity(i);
            }
        });

        txtlienket_nganhang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtstkkk = txtstk.getText().toString();
                if(txtstkkk.equals("")) {
                    Intent i = new Intent(getApplicationContext(), LienKetBank1.class);
                    Bundle bs = new Bundle();
                    bs.putSerializable("TaiKhoang", tkCurrent);
                    i.putExtras(bs);
                    startActivity(i);
                }
                else
                    Toast.makeText(getApplicationContext(),"Bạn đã liên kết ngân hàng trước đó",Toast.LENGTH_LONG).show();
            }
        });
     }

}
