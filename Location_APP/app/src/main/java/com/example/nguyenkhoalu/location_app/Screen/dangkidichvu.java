package com.example.nguyenkhoalu.location_app.Screen;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nguyenkhoalu.location_app.R;

public class dangkidichvu extends Activity {


    Button btndkccdv;
    EditText edttenchuccdv,edtcmndccdv,edtstkccdv,edtadressccdv,edttypeccdv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangkidichvu);

        btndkccdv = (Button) findViewById(R.id.btndkccdv);
        edttenchuccdv = (EditText) findViewById(R.id.edttenchuccdv);
        edtcmndccdv = (EditText) findViewById(R.id.edtcmndccdv);
        edtstkccdv = (EditText) findViewById(R.id.edtstkccdv);
        edtadressccdv = (EditText) findViewById(R.id.edtadressccdv);


        final SQLite sqLite = new SQLite(this,"quanly.sqlite",null,1);
        // TẠO BẢNG
        sqLite.query("CREATE TABLE IF NOT EXISTS DICHVU(ID INTEGER PRIMARY KEY AUTOINCREMENT,TENCHU VARCHAR,CMND VARCHAR,STK VARCHAR,DIACHI VARCHAR,LOAIHINH VARCHAR)");

        btndkccdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tenchuccdv,cmndccdv,stkccdv,adressccdv,typeccdv;
                tenchuccdv = edttenchuccdv.getText().toString();
                cmndccdv = edtcmndccdv.getText().toString();
                stkccdv = edtstkccdv.getText().toString();
                adressccdv = edtadressccdv.getText().toString();
                typeccdv = edttypeccdv.getText().toString();

                if(tenchuccdv.equals("")||cmndccdv.equals("")||stkccdv.equals("")||adressccdv.equals("")||typeccdv.equals(""))
                    Toast.makeText(getApplicationContext(),"Thông tin chưa được nhập đầy đủ",Toast.LENGTH_LONG).show();
                else
                {
                    String sql = String.format("INSERT INTO DICHVU VALUES(null,'%s','%s','%s','%s','%s')",tenchuccdv, cmndccdv,stkccdv,adressccdv,typeccdv);
                    sqLite.query(sql);
                    Toast.makeText(getApplicationContext(), "Đăng ký dịch vụ thành công", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
