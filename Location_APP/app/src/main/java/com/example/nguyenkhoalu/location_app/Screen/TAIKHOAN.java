package com.example.nguyenkhoalu.location_app.Screen;

import java.io.Serializable;

/**
 * Created by LEHUULY on 4/30/2018.
 */

public class TAIKHOAN implements Serializable{
    int matk;
    private String username;
    private String passwword;
    private String name;
    private String CMND;
    private String NumofAcount;
    private String Email;

    public TAIKHOAN(int matk, String username, String passwword, String name, String CMND, String numofAcount, String email) {
        this.matk = matk;
        this.username = username;
        this.passwword = passwword;
        this.name = name;
        this.CMND = CMND;
        this.NumofAcount = numofAcount;
        this.Email = email;
    }

    public TAIKHOAN(int matk, String username, String passwword, String name, String CMND, String numofAcount) {
        this.matk = matk;
        this.username = username;
        this.passwword = passwword;
        this.name = name;
        this.CMND = CMND;
        NumofAcount = numofAcount;
    }

    public TAIKHOAN() {}

    public int getMatk() {
        return matk;
    }

    public void setMatk(int matk) {
        this.matk = matk;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswword() {
        return passwword;
    }

    public void setPasswword(String passwword) {
        this.passwword = passwword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

    public String getNumofAcount() {
        return NumofAcount;
    }

    public void setNumofAcount(String numofAcount) {
        NumofAcount = numofAcount;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    @Override
    public String toString() {
        return "TAIKHOAN{" +
                "matk=" + matk +
                ", username='" + username + '\'' +
                ", passwword='" + passwword + '\'' +
                ", name='" + name + '\'' +
                ", CMND='" + CMND + '\'' +
                ", NumofAcount='" + NumofAcount + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }
}
